var express = require('express');

var port = 8999;

var app = express.createServer();

function checkAuth (req, res, next) {
	console.log('checkAuth ' + req.url);

// the following code are retrieved from the following website for learning the nodejs relationship and basic authentication
//https://gist.github.com/smebberson/1581536


	// don't serve /secure to those not logged in
	// you should add to this list, for each and every secure url
	if (req.url === '/secure' && (!req.session || !req.session.authenticated)) {
		res.render('unauthorised', { status: 403 });
		return;
	}

	next();
}

app.configure(function () {

	app.use(express.cookieParser());
	app.use('/picture', express.static('picture'))
	app.use(express.session({ secret: 'example' }));
	app.use(express.bodyParser());
	app.use(checkAuth);
	app.use(app.router);
	app.set('view engine', 'jade');
	app.set('view options', { layout: false });

});


require('./lib/routes.js')(app);

app.listen(port);
console.log('Node listening on port %s', port);
