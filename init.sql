CREATE DATABASE mydb;

USE mydb;

CREATE TABLE users (id int(100) NOT NULL AUTO_INCREMENT,
 email varchar(255) NOT NULL,
 password varchar(255) NOT NULL,
 //role 
 PRIMARY KEY (id)) AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO users (email, password)
VALUES ('abc@gmail.com','123456');

INSERT INTO users (email, password)
VALUES ('admin@gmail.com','56789');

CREATE TABLE resource (id int(100) NOT NULL AUTO_INCREMENT,
 year int(11) NOT NULL,
 title varchar(255) NOT NULL,
 person varchar(255) NOT NULL,
 userId long NOT NULL,
 PRIMARY KEY (id)) AUTO_INCREMENT=1 CHARSET=utf8;

#//这里你看下你数据库里的users表里的id是否是8
insert into resource (year,title,person,userId) values(2011,"Good book","Jack",1);

#//select details by years
SELECT year,COUNT(*) FROM `resource` GROUP BY YEAR;

CREATE TABLE uploadData (id int(100) NOT NULL AUTO_INCREMENT,
 year int(11) NOT NULL,
 title varchar(255) NOT NULL,
 person varchar(255) NOT NULL,
 userId long NOT NULL,
 PRIMARY KEY (id)) AUTO_INCREMENT=1 CHARSET=utf8;

 insert into uploadData (year,title,person,userId) values ('"+year+"', '"+title+"', '"+person+"','"+userId+"')