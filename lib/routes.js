var util = require('util');
var http = require('http');
var express = require('express');
var mysql = require('mysql');
var connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'MyNewPass',
	database: 'mydb'
});
connection.connect();
/*
connection.connect(function(err){
if(!err) {
    console.log("Database is connected");
} else {
    console.log("Error while connecting with database");
}
});
*/

//module.exports = connection;

module.exports = function (app) {

	app.get('/', function (req, res, next) {
		res.render('index');
	});

	app.get('/welcome', function (req, res, next) {
		res.render('welcome');
	});

	app.get('/upload', function (req, res, next) {
		res.render('upload');
	});

	app.get('/secure', function (req, res, next) {
		res.render('secure');
	});

	app.post('/upload', function (req, res, next) {
		var fs = require("fs");
		const parse = require('csv-parse');
		var userInfo = req.session.userInfo;
		var userId = userInfo.userId;

		//var csv = require('fast-csv');
		//var fs = require("express-fileupload");
	//	console.log(res);


		fs.readFile(req.files.csvfile.path, function (err, data) {

			function MyCSV(Year, Title, Person) {
				this.Year = Year;
				this.Title = Title;
				this.Person = Person;

			};
			/*
			//var tables = new Array();	
			var tables = [];	

			for (var index = 0; index< data.length; index++) {
						
						tables.push(new MyCSV(data[index][0], data[index][1], data[index][2]));

					}

			*/
			if (err) {
				console.log(err.stack);
				return;
			}
			/*
			var array = data.toString().split("\n");
			var testing = array.toString().split(",");
			for(i in array) {
		//		console.log(testing[0]);
			}
			*/
			var example = new Array();

			parse(data, function (err, output) {
				//console.log(output[1][1]); // 'a'
				// todo: check first row is correct headings, then remove it
				if (output[0][0] == 'Year' && output[0][1] == 'Title' && output[0][2] == 'Person') {
					console.log("=========++");
					var heading = output.shift();
					console.log(output);
				}
				// todo: go over all the rows,
				// check they all have 3 cells (if not show error message),
				// push userId as fourth cell
				var newOuput = [];
				for (var i = 0 ; i < output.length; i ++) {
					if (output[i].length == 3) {
						var temp = output[i];
						temp.push(userId);
						newOuput.push(temp);
					}
				}
				var insertSQL = "insert into uploadData (year,title,person,userId) values ?";
				connection.query(insertSQL, [newOuput], function (err, result) {
					if (err) {
						console.log(err);
						// todo: show an error message
					} else {
						res.redirect("/resource");
					}
				});
			/*
				for (var index = 0; index < output.length; index++) {

					example.push(new MyCSV(output[index][0], output[index][1], output[index][2]));
					var selectSQL = "insert into uploadData (year,title,person,userId) values ('" + output[index][0] + "', '" + output[index][1] + "', '" + output[index][2] + "','" + userId + "')";


					console.log(selectSQL);
					connection.query(selectSQL, function (err, result) {

						if (err) {
							console.log(err);

						} else {

							res.redirect("/resource");

						}
						//		console.log(example[0]);
					});
				}
				*/
			});
		});
	});

	app.get('/resource', function (req, res, next) {
		if (req.session.userInfo) {
			var userInfo = req.session.userInfo;
			var userEmail = userInfo.userEmail;
			var userId = userInfo.userId;

			var selectSQL = "select * from resource WHERE userID ='" + userInfo.userId + "' order by id desc";
			connection.query(selectSQL, function (err, result) {
				console.log("the resource is === " + JSON.stringify(result));
				console.log("the user ID is " + userId);

				for (var i = 0; i < result.length; i++) {
					var testing = result[i]['year'];
					console.log(result[i]['year']);
				}
				res.render('resource', { userId: userId, userEmail: userEmail, newspaperData: result });
				if (err) {
					console.log(err);

				}
			});
		} else {
			res.render('login');
		}
	});

	app.get('/register', function (req, res, next) {
		res.render('register', { title: 'Registration form' });
	});

	app.post('/register', function (req, res, next) {
		var email = req.body.email;
		var password = req.body.password;

		var selectSQL = "insert into users (email,password,role) values (?, ?, 'user')";
		console.log(selectSQL);
		connection.query(selectSQL, [email, password], function (err, result) {

			if (err) {
				console.log(err);

			} else {

				res.redirect("/admin");
			}

		})
	});
	app.get('/login', function (req, res, next) {
		res.render('login', { flash: req.flash() });
	});

	app.get('/admin', function (req, res, next) {
		if (req.session.userInfo) {
			var userInfo = req.session.userInfo;
			var userEmail = userInfo.userEmail;
			var userId = userInfo.userId;

			// todo: if userInfo.role is not "admin", show an error page
			//	res.send("email: " + userEmail);

			var selectSQL = "SELECT year,COUNT(*) AS yearCount FROM `resource` GROUP BY year ";
			connection.query(selectSQL, function (err, count) {
				console.log("the total count is === " + JSON.stringify(count));
				console.log("the user ID is " + userId);
				res.render('admin', { totalCount: count });
				if (err) {
					console.log(err);

				}
			});
		} else {
			res.render('admin');
		}
	});

	app.post('/register', (req, res) => {
		res.render('register', { title: 'Registration form' });

	});

	app.post('/login', function (req, res, next) {
		var email = req.body.email;
		var password = req.body.password;
		var adminEmail = 'admin@gmail.com';
		console.log("email is===" + email);
		var selectSQL = "select * from users where email = '" + email + "' ";

		connection.query(selectSQL, function (err, rows) {
			console.log("the result is === " + rows);

			if (err) {
				console.log(err);
			} else {
				if (rows.length == 0) {
					res.send("用户名不存在");
				} else if (rows[0].password == req.body.password) {
					//登录成功，返回用户的全部信息
					req.session.authenticated = true;
					let data = {};
					data.userEmail = rows[0].email;
					data.userId = rows[0].id;
					// todo: also get role and save in session
					//登陆成功后设置session,把用户的数据设置在session中
					req.session.userInfo = data;
					console.log("session userEmail" + req.session.userInfo.userEmail);
					console.log("session userId" + req.session.userInfo.userId);

					if (email == adminEmail) {
						res.redirect('/admin');
					} else {
						res.redirect('/secure');
					}
				} else {
					res.send("密码错误");
				}
			}
		})

	});

	app.get('/create', function (req, res, next) {
		res.render('create', { flash: req.flash() });
	});

	app.post('/create', function (req, res, next) {
		if (req.session.userInfo) {
			var userInfo = req.session.userInfo;
			var userId = userInfo.userId;
			var year = req.body.year;
			var title = req.body.title;
			var person = req.body.person;

			var selectSQL = "insert into resource (year,title,person,userId) values ('" + year + "', '" + title + "', '" + person + "','" + userId + "')";
			connection.query(selectSQL, function (err, result) {

				if (err) {
					console.log(err);

				} else {
					console.log(result.year + " " + result.title + " " + result.person + " ");
					console.log("you have keyedin" + result.year);
					//res.send("success");
					res.redirect("/resource");
				}

			})
		}
	});

	app.get('/toEdit/:id', function (req, res, next) {
		console.log("requst is ==== '" + req + "'");
		console.log("requst params is ==== '" + req.params + "'");

		var id = req.params.id;
		console.log("id is ==== '" + id + "'");
		var sql = "SELECT * from resource where id  = '" + id + "'";

		connection.query(sql, function (err, result) {

			if (err) {
				console.log(err);

			} else {

				res.render('edit', { result: result });
			}

		});
	});

	app.post('/edit', function (req, res) {
		var id = req.body.id;
		var year = req.body.year;
		var title = req.body.title;
		var person = req.body.person;

		var sql = "update resource set year = '" + year + "', title='" + title + "', person='" + person + "'where id = '" + id + "' ";

		connection.query(sql, function (err, result) {

			if (err) {
				console.log(err);
			} else {

				res.redirect("/resource");
			}
		});

	});

	app.get('/delete/:id', function (req, res) {
		var id = req.params.id;
		console.log("ID is ===" + id);
		var sql = "delete FROM resource where id = '" + id + "' ";
		connection.query(sql, function (err, result) {

			if (err) {
				console.log(err);
			} else {

				res.redirect("/resource");
			}
		});
	});

	app.get('/logout', function (req, res, next) {
		delete req.session.authenticated;
		res.redirect('/');
	});

};
